import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ListView from './screens/ListView';
import DetailView from './screens/DetailView';
import LoginView from './screens/LoginView';

const Stack = createStackNavigator();

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login" component={LoginView}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            options={{
              headerStyle: {
                backgroundColor: '#03c2fc',
              },
              headerTintColor: '#fff'
            }}
            name="Lista de cubos" component={ListView} />
          <Stack.Screen
            options={{
              headerStyle: {
                backgroundColor: '#03c2fc',
              },
              headerTintColor: '#fff'
            }}
            name="Detalle" component={DetailView} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

